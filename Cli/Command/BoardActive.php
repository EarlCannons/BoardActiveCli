<?php
/**
 * Created by someone.
 * User: unknown
 * Date: 11/30/20
 * Time: 2:38 PM
 */

namespace Earl\BoardActiveCli\Cli\Command;


use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class BoardActive extends Command
{
    protected function configure()
    {
        $this->setName('xf:board-active')
            ->setDescription('Turn your board on and off')
            ->addArgument(
                'switch',
                InputArgument::REQUIRED,
                'On or Off'
            )
            ->addOption(
                'message',
                'm',
                InputOption::VALUE_OPTIONAL,
                'When the board is inactive / closed, this message will be shown to site visitors'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $switch = $input->getArgument('switch');
        $allowedSwitches = ['on', 'off'];

        if (!in_array($switch, $allowedSwitches)) throw new \InvalidArgumentException("Unknown switch: $switch");

        \XF::repository('XF:Option')->updateOptions([
            'boardActive' => $switch == 'on',
            'boardInactiveMessage' => $input->getOption('message') ? $input->getOption('message') : \XF::options()->boardInactiveMessage,
        ]);
        $output->writeln('<info>The board has been turned ' . $input->getArgument('switch') . '.</info>');

    }
}